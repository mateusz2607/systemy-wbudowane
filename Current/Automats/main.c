#include "led.h"
#include "keyboard.h"

int main(){
  
  enum LedsState{MOVE_LEFT, MOVE_RIGHT};
  enum LedsState eLedsState = MOVE_LEFT;

  char cStepsCounter = 0;
  
  LedInit();
  while(1){
    switch(eLedsState){
      case MOVE_LEFT:
        cStepsCounter++;
        if (cStepsCounter == 3){
          eLedsState = MOVE_RIGHT;
        }
        LedStepLeft();
        break;
      case MOVE_RIGHT:
        cStepsCounter--; 
        if (cStepsCounter == 0){
          eLedsState = MOVE_LEFT;
        }
        LedStepRight();
        break;
    }
    delay(50);
  }
}
