enum LEDDIRECTION {LEFT, RIGHT};

void LedOn(unsigned char ucLedIndeks);
void LedInit(void);
void LedStep(unsigned char ucDirection);
void LedStepLeft(void);
void LedStepRight(void);
void delay(unsigned int uiTimeMs);
