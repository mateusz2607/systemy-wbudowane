#include "led.h"
#include "keyboard.h"

int main(){
  
  LedInit();
  while(1){
    switch (eKeyboardRead()){
      case 0:
        LedStepRight();
        break;
      case 1:
        LedStepLeft();
        break;
    }
    delay(100);
  }
}
