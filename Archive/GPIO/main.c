#include <LPC21xx.H>


#define LED0_bm 0x00010000
#define LED1_bm 0x00020000
#define LED2_bm 0x00040000
#define LED3_bm 0x00080000

#define BUT1_bm 0x10
#define BUT2_bm 0x40
#define BUT3_bm 0x20
#define BUT4_bm 0x80

#define TIME_MULTIPLIER 4997 


enum LEDDIRECTION {LEFT, RIGHT};
enum KeyboardState {BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_4, RELASED};


void delay(unsigned int uiTimeMs){

	unsigned long int uliIterationCounter;
  unsigned char ucNumber; 

	for(uliIterationCounter=0;uliIterationCounter<=TIME_MULTIPLIER*uiTimeMs;uliIterationCounter++){
		ucNumber++;
	}
}


void LedOn(unsigned char ucLedNumber){

  static unsigned int uiLedsState;

  if(uiLedsState != ucLedNumber){
    IO1CLR=IO1CLR|LED0_bm|LED1_bm|LED2_bm|LED3_bm;
  }
  switch (ucLedNumber){
    case 0:
      uiLedsState=0;
      IO1SET=LED0_bm;
      break;
    case 1:
      uiLedsState=1;
      IO1SET=LED1_bm;
      break;
    case 2:
      uiLedsState=2;
      IO1SET=LED2_bm;
      break;
    case 3:
      uiLedsState=3;
      IO1SET=LED3_bm;
      break;
    case 4:
      uiLedsState=4;
      IO1CLR=IO1CLR|LED0_bm|LED1_bm|LED2_bm|LED3_bm;
      break;
  }
}


void KeyboardInit(void){

  IO0DIR=IO0DIR&(~(BUT1_bm|BUT2_bm|BUT3_bm|BUT4_bm));
}


void LedInit(void){

  IO1DIR=IO1DIR|LED0_bm|LED1_bm|LED2_bm|LED3_bm; 
  LedOn(0);
}


int eKeyboardRead(void){
  
  enum KeyboardState eStateToReturn=RELASED;
  
  if (~IO0PIN & BUT1_bm){
    eStateToReturn=BUTTON_1;
  }
  else if (~IO0PIN & BUT2_bm){
    eStateToReturn=BUTTON_2;
  }
  else if (~IO0PIN & BUT3_bm){
    eStateToReturn=BUTTON_3;
  }
  else if (~IO0PIN & BUT4_bm){
    eStateToReturn=BUTTON_4;
  }

  return eStateToReturn;
} 


void LedStep(unsigned char ucDirection){

  static unsigned int uiActualPosition;

  switch (ucDirection){
    case LEFT:
      uiActualPosition++;
      break;
    case RIGHT:
      uiActualPosition--;
      break;
  }
  LedOn(uiActualPosition%4);
}


void LedStepLeft(void){

  LedStep(LEFT);
}


void LedStepRight(void){

  LedStep(RIGHT);
}


int main(){
  
  LedInit();
	while(1){
    switch (eKeyboardRead()){
      case BUTTON_1:
        LedStepRight();
        break;
      case BUTTON_2:
        LedStepLeft();
        break;
    }
    delay(100);
	}
}
